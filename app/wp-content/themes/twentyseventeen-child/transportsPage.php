<?php /* Template Name: Transports Page */
get_header();
the_breadcrumb();
?>
<section id="transports" class="page-section" role="main">
    <div class="wrap">
        <div id="primary" class="content-area">
            <?php get_template_part('template-parts/page/content', 'transports'); ?>
        </div>
    </div>
</section>
<section id="services" class="page-section" role="main">
    <div class="wrap">
        <div id="primary" class="content-area">
            <?php get_template_part('template-parts/page/content', 'services'); ?>
        </div>
    </div>
</section>
<?php
get_footer();
?>