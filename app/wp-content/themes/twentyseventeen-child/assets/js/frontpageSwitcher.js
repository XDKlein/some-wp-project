/* global twentyseventeenScreenReaderText */
/**
 * Theme functions file.
 *
 * Contains handlers for navigation and widget area.
 */

(function( $ ) {
    $(document).ready( function() {
        const switcher = $('.front-page-switcher');

        $('.front-page-switcher-tab').on('click', function (e) {
            if(!$(this).hasClass('active-tab')) {
                $(switcher).find('.active-tab').removeClass('active-tab');
                $(this).addClass('active-tab');
            }
        });
    });
})( jQuery );
