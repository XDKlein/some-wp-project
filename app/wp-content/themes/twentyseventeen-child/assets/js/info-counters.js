/* global twentyseventeenScreenReaderText */
/**
 * Theme functions file.
 *
 * Contains handlers for navigation and widget area.
 */

(function( $ ) {
    $(document).ready( function() {
        const counters = $('.info-counter-block');

        $(counters).each((index, counter) => {
            const value = $(counter).find('.info-counter-value'),
                target = $(value).data('target');
            value.css('opacity', '1');
            $({countNum: target / 2}).animate({
                    countNum: target
                },
                {
                    duration: 2500,
                    easing:'swing',
                    step: function() {
                        value.text(Math.floor(this.countNum));
                    },
                    complete: function() {
                        value.text(this.countNum);
                    }
                });
        });
    });
})( jQuery );
