/* global twentyseventeenScreenReaderText */
/**
 * Theme functions file.
 *
 * Contains handlers for navigation and widget area.
 */

(function( $ ) {
    const $slider = $('.header-slider');
    let slides = [];

    $(document).ready( function() {
        slides = iterate($slider.find('.slide').get().map(slide => {
            return {
                _selector: $(slide),
                show: () => {
                    $(slide).addClass('active');
                    $slider.find('.slider-background').fadeOut(1000, function() {this.remove()});
                    $('<div class="slider-background"></div>').prependTo('.slider-wrapper').css('background-image', `url(${$(slide).data('background')})`)
                },
                hide: () => {
                    $(slide).removeClass('active');
                },
            }
        }));
        slides.init(5000);
    });

    function iterate(array) {
        return {
            _i: 0,
            _array: array,
            _interval: null,
            _timer: 0,
            init(timer) {
                this._timer = timer;
                this.resetInterval();
                this.current().show();
            },
            resetInterval() {
                clearInterval(this._interval);
                this._interval = setInterval(() => {
                    this.current().hide(); this.next().show()
                }, this._timer);
            },
            current() {
                return this._array[this._i]
            },
            next() {
                this.resetInterval();
                this._i = this._i + 1 < this._array.length ? this._i + 1 : 0;
                return this._array[this._i];
            },
            previous() {
                this.resetInterval();
                this._i = this._i - 1 >= 0 ? this._i - 1 : this._array.length - 1;
                return this._array[this._i];
            }
        }
    }

    $('.slide-next').on('click', function (e) {
        slides.current().hide();
        slides.next().show();
    });

    $('.slide-prev').on('click', function (e) {
        slides.current().hide();
        slides.previous().show();
    })
})( jQuery );
