<section id="info-counters">
    <div class="info-counters-header">
        <h2>Гарантии вашего успеха</h2>
        <span>*Ежедневные аналитические данные по результатам деятельности компании за 2020 год</span>
    </div>
    <div class="info-counters-container">
        <div class="info-counter-block">
            <div class="info-counter-icon"><i class="fas fa-globe"></i></div>
            <div class="info-counter-value" data-target=154.6>0</div>
            <div class="info-counter-description">Млн. километров</div>
        </div>
        <div class="info-counter-block">
            <div class="info-counter-icon"><i class="fas fa-user-check"></i></div>
            <div class="info-counter-value" data-target=76219>0</div>
            <div class="info-counter-description">Выполненных заказов</div>
        </div>
        <div class="info-counter-block">
            <div class="info-counter-icon"><i class="fas fa-map-pin"></i></div>
            <div class="info-counter-value" data-target=1056>0</div>
            <div class="info-counter-description">Целевых трейд-лейнов</div>
        </div>
        <div class="info-counter-block">
            <div class="info-counter-icon"><i class="fas fa-handshake"></i></i></div>
            <div class="info-counter-value" data-target=3666>0</div>
            <div class="info-counter-description">Корпоративных клиентов</div>
        </div>
        <div class="info-counter-block">
            <div class="info-counter-icon"><i class="fas fa-users"></i></div>
            <div class="info-counter-value" data-target=9581>0</div>
            <div class="info-counter-description">Надежных подрядчиков</div>
        </div>
        <div class="info-counter-block">
            <div class="info-counter-icon"><i class="fas fa-birthday-cake"></i></div>
            <div class="info-counter-value" data-target=25>0</div>
            <div class="info-counter-description">Лет компании</div>
        </div>
    </div>
</section>
