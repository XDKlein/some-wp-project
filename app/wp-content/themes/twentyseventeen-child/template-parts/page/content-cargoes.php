<?php //TODO meld cargoes into transports
$type = is_front_page() ? 'short' : 'full';
$post = get_post(getFirstPostOf('cargoesPage')); //TODO replace this with prettier find function
$postContent = apply_filters('process_content', $post->post_content, $type);
$children = get_pages('child_of='.$post->ID);
$cargoes = [];
foreach($children as $child) {
    $childPost = get_post($child->ID);
    array_push($cargoes, (object) [
        'ID' => $child->ID,
        'content' => apply_filters('process_content', $childPost->post_content, 'short')
    ]);
}
?>
<?php if($postContent->title || $postContent->description) { ?>
    <div class="page-section-title">
        <h1><?php echo $postContent->title ?></h1>
        <div class="page-section-description">
            <?php echo $postContent->description ?>
        </div>
    </div>
<?php } ?>
<div class="preview-blocks">
    <?php
    foreach ($cargoes as $cargo) {
        ?>
        <div class="preview-block">
            <div class="preview-block-image">
                <?php echo get_the_post_thumbnail( $cargo->ID, 'medium') ?>
            </div>
            <div class="preview-block-description" style="height: unset">
                <div class="preview-block-description-title"><h3><?php echo $cargo->content->title ?></h3></div>
                <?php if($cargo->content->description) { ?>
                    <div class="preview-block-description-text"><?php echo $cargo->content->description ?></div>
                <? } ?>
            </div>
        </div>
        <?php
    }
    ?>
</div>