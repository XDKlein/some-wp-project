<div class="enquiry-form">
    <div class="enquiry-form-info">
        <h3>Запрос на перевозку груза</h3>
        <p>К перевозке принимаются грузы от 1 паллеты и/или свыше 500 кг. Для транспортировки товаров до 500 кг воспользуйтесь услугами перевозки сборных грузов или авиадоставкой. Компания не занимается перевозкой личных вещей.</p>
        <p class="notification">*Обращаем ваше внимание, что компания не работает с физическими лицами, индивидуальными предпринимателями.</p>
    </div>
    <div class="enquiry-form-separator">
        <div class="separator-first-point"><i class="fas fa-map-pin"></i></div>
        <div class="separator-second-point"><i class="fas fa-map-pin"></i></div>
        <div class="separator-line"></div>
    </div>
    <div class="enquiry-form-main">
        <div class="enquiry-form-main-inputs">
            <div class="enquiry-form-input">
                <div class="enquiry-form-input-icon"><label><i class="fas fa-id-card"></i></label></div>
                <input type="text" name="company" class="required" placeholder="Компания*">
            </div>
            <div class="enquiry-form-input">
                <div class="enquiry-form-input-icon"><label><i class="fas fa-user"></i></label></div>
                <input type="text" name="name" class="required" placeholder="Имя/Фамилия*">
            </div>
            <div class="enquiry-form-input">
                <div class="enquiry-form-input-icon"><label><i class="fas fa-phone-alt"></i></label></div>
                <input type="text" name="phone" class="required" placeholder="Телефон*">
            </div>
            <div class="enquiry-form-input">
                <div class="enquiry-form-input-icon"><label><i class="fas fa-at"></i></label></div>
                <input type="text" name="email" class="required" placeholder="Email*">
            </div>
        </div>
        <a class="enquiry-form-submit" alt="В процессе разработки">Далее</a>
    </div>
</div>