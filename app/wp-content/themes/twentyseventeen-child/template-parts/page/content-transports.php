<?php
$type = is_front_page() ? 'short' : 'full';
$post = get_post(getFirstPostOf('transportsPage')); //TODO replace this with prettier find function
$postContent = apply_filters('process_content', $post->post_content, $type);
$children = get_pages(["child_of" => $post->ID, "meta_key" => '_wp_page_template', "meta_value" => "transportPage.php"]);
$transports = [];
foreach($children as $child) {
    $childPost = get_post($child->ID);
    array_push($transports, (object) [
        'ID' => $child->ID,
        'content' => apply_filters('process_content', $childPost->post_content, 'short')
    ]);
}
?>
<div class="page-section-title">
    <h1><?php echo $postContent->title ?></h1>
    <div class="page-section-description">
        <?php echo $postContent->description ?>
    </div>
</div>
<div class="preview-blocks">
    <?php
    foreach ($transports as $transport) {
        ?>
        <div class="preview-block">
            <div class="preview-block-image">
                <?php echo get_the_post_thumbnail( $transport->ID, 'medium') ?>
            </div>
            <div class="preview-block-description">
                <div class="preview-block-description-title"><h3><?php echo $transport->content->title ?></h3></div>
                <div class="preview-block-description-text"><?php echo $transport->content->description ?></div>
            </div>
        </div>
        <?php
    }
    ?>
</div>