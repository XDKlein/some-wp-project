<?php
$type = is_front_page() ? 'short' : 'full';
$post = get_post(getFirstPostOf('transportsPage')); //TODO replace this with prettier find function
$postContent = apply_filters('process_content', $post->post_content, $type);
$children = get_pages(["child_of" => $post->ID, "meta_key" => '_wp_page_template', "meta_value" => "servicePage.php"]);
$services = [];
foreach($children as $child) {
    $childPost = get_post($child->ID);
    array_push($services, (object) [
        'ID' => $child->ID,
        'content' => apply_filters('process_content', $childPost->post_content, 'short')
    ]);
}
?>
<div class="preview-blocks">
    <?php
    foreach ($services as $service) {
        ?>
        <div class="preview-block">
            <div class="preview-block-image">
                <?php echo get_the_post_thumbnail( $service->ID, 'medium') ?>
            </div>
            <div class="preview-block-description">
                <div class="preview-block-description-title"><h3><?php echo $service->content->title ?></h3></div>
                <div class="preview-block-description-text"><?php echo $service->content->description ?></div>
            </div>
        </div>
        <?php
    }
    ?>
</div>