<div class="header-slider">
    <div class="slider-wrapper">
        <div class="slider-overlay"></div>
        <div class="slider-background"></div>
        <a class="slide-controls slide-prev">
            <div class="slide-controls-overlay"></div>
            <div class="slide-controls-symbol">
                <i class="fas fa-chevron-left"></i>
            </div>
        </a>
        <div class="slide" data-background="https://images2.alphacoders.com/947/thumb-1920-947077.jpg">
            <div class="slide-title"><h1>Some slide 1</h1></div>
            <div class="slide-description">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Enim neque volutpat ac tincidunt vitae semper quis lectus nulla. Eu scelerisque felis imperdiet proin fermentum. Sem nulla pharetra diam sit amet nisl suscipit adipiscing bibendum. Tellus rutrum tellus pellentesque eu tincidunt. Eget lorem dolor sed viverra ipsum nunc aliquet bibendum enim. Arcu felis bibendum ut tristique et egestas. Pretium lectus quam id leo in. Purus sit amet luctus venenatis lectus. Placerat vestibulum lectus mauris ultrices eros in cursus turpis.
            </div>
            <a class="slide-button">Подробнее</a>
        </div>
        <div class="slide" data-background="https://img1.goodfon.ru/original/1920x1200/8/6d/art-core-of-the-universe.jpg">
            <div class="slide-title"><h1>Some slide 2</h1></div>
            <div class="slide-description">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Enim neque volutpat ac tincidunt vitae semper quis lectus nulla. Eu scelerisque felis imperdiet proin fermentum. Sem nulla pharetra diam sit amet nisl suscipit adipiscing bibendum. Tellus rutrum tellus pellentesque eu tincidunt. Eget lorem dolor sed viverra ipsum nunc aliquet bibendum enim. Arcu felis bibendum ut tristique et egestas. Pretium lectus quam id leo in. Purus sit amet luctus venenatis lectus. Placerat vestibulum lectus mauris ultrices eros in cursus turpis.
            </div>
            <a class="slide-button">Подробнее</a>
        </div>
        <div class="slide" data-background="https://www.ubackground.com/_ph/86/86333091.jpg">
            <div class="slide-title"><h1>Some slide 3</h1></div>
            <div class="slide-description">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Enim neque volutpat ac tincidunt vitae semper quis lectus nulla. Eu scelerisque felis imperdiet proin fermentum. Sem nulla pharetra diam sit amet nisl suscipit adipiscing bibendum. Tellus rutrum tellus pellentesque eu tincidunt. Eget lorem dolor sed viverra ipsum nunc aliquet bibendum enim. Arcu felis bibendum ut tristique et egestas. Pretium lectus quam id leo in. Purus sit amet luctus venenatis lectus. Placerat vestibulum lectus mauris ultrices eros in cursus turpis.
            </div>
            <a class="slide-button">Подробнее</a>
        </div>
        <a class="slide-controls slide-next">
            <div class="slide-controls-overlay"></div>
            <div class="slide-controls-symbol">
                <i class="fas fa-chevron-right"></i>
            </div>
        </a>
    </div>
</div>
