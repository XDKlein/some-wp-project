<?php
/**
 * Displays header media
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<div class="custom-header">

		<div class="custom-header-media">
			<?php the_custom_header_markup(); ?>
		</div>
    <div class="top-bar">
        <div class="button">
            Вакансии
        </div>
        <div class="top-bar-list">
            <div>
                <span class="top-bar-icon flaticon-phone41"></span>
                <span class="top-bar-item" itemprop="telephone">
                <a href="tel:+375 29 000 00 00" onclick="gtag('event', 'click', {'event_category' : 'button','event_label':'Tel',}); ym(13723366, 'reachGoal', 'Tel'); return true;" target="_blank">+375 29 000 00 00</a>
            </span>
            </div>
            <div>
                <span class="top-bar-icon flaticon-mail4"></span>
                <span class="top-bar-item" itemprop="email">
                <a href="mailto:email@email.email" onclick="gtag('event', 'click', {'event_category' : 'button','event_label':'Mailto',}); ym(13723366, 'reachGoal', 'Mailto'); return true;">email@email.email</a>
            </span>
            </div>
            <div>
                <span class="top-bar-icon flaticon-clock96"></span>
                <span class="top-bar-item">
                <a>Пн - Пт 09:00 - 18:00</a>
            </span>
            </div>
        </div>
    </div>

	<?php get_template_part( 'template-parts/header/site', 'branding' ); ?>

</div><!-- .custom-header -->
