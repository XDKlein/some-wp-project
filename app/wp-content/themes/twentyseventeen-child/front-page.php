<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<?php
        get_template_part('template-parts/page/content', 'info-counters');
        ?>
        <section class="page-section front-page" role="main">
            <div class="wrap">
                <div id="primary" class="content-area">
                    <?php get_template_part('template-parts/page/content', 'transports'); ?>
                </div>
            </div>
        </section>

        <!-- Switcher -->
        <section class="page-section front-page" role="main">
            <div class="wrap">
                <div id="primary" class="content-area">
                    <div class="front-page-switcher">
                        <div class="front-page-switcher-tabs">
                            <div class="front-page-switcher-tab active-tab" data-tab="0">Специальные грузы</div>
                            <div class="front-page-switcher-tab" data-tab="1">Отраслевые решения</div>
                        </div>
                        <div class="front-page-switcher-content active-content">
                            <?php get_template_part('template-parts/page/content', 'cargoes'); ?>
                        </div>
                        <div class="front-page-switcher-content">
                            <?php get_template_part('template-parts/page/content', 'cargoes'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="page-section front-page" role="main">
            <div class="wrap">
                <div id="primary" class="content-area">
                    <div class="row">
                        <div class="column-6">
                            <div class="front-page-services">
                                <h3>Дополнительные услуги</h3>
                                <div class="front-page-services-content">
                                    <?php get_template_part('template-parts/page/content', 'services'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="column-4">
                            <?php get_template_part('template-parts/page/content', 'enquiry-form'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
	</main>
</div>

<?php get_footer();
