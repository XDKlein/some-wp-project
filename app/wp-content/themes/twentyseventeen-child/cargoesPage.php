<?php /* Template Name: Cargoes Page */
get_header();
the_breadcrumb();
?>
<section id="cargoes" class="page-section" role="main">
    <div class="wrap">
        <div id="primary" class="content-area">
            <?php get_template_part('template-parts/page/content', 'cargoes'); ?>
        </div>
    </div>
</section>
<?php
get_footer();
?>